using UnityEngine;
using UnityEngine.UI;

namespace TMGameTest
{
    public class InputFieldSubmitter : MonoBehaviour
    {
        [SerializeField]
        private InputField _inputField;
        [SerializeField]
        private UnityStringEvent _submitEvent;

        private void OnGUI()
        {
            if (_inputField.isFocused && Input.GetKey(KeyCode.Return))
                Submit();
        }

        private bool _isSubmitting;

        public void Submit()
        {
            if (_isSubmitting)
                return;
            if (!string.IsNullOrEmpty(_inputField.text))
            {
                _isSubmitting = true;
                _submitEvent?.Invoke(_inputField.text);
            }
        }

        public void SentSuccessfull()
        {
            clearInputField();
            _isSubmitting = false;
        }

        public void SentFail()
        {
            _isSubmitting = false;
        }

        private void clearInputField()
        {
            _inputField.text = string.Empty;
        }
    }
}
