using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TMGameTest
{
    public class ChatView : MonoBehaviour
    {
        [SerializeField]
        private UnityStringEvent _onMessageHasCome;
        [SerializeField]
        private UnityStringEvent _onMessageSendingRequested;
        [SerializeField]
        private UnityEvent _onMessageSentSuccessfully;
        [SerializeField]
        private UnityEvent _onMessageSendingFailed;

        public void MessageSendingRequested(string message)
        {
            _onMessageSendingRequested?.Invoke(message);
        }

        public void OnMessageSendingResultHasCome(ServerResult result)
        {
            switch (result)
            {
                case ServerResult.NotFound:
                    _onMessageSendingFailed?.Invoke();
                    break;
                case ServerResult.OK:
                    _onMessageSentSuccessfully?.Invoke();
                    break;
            }
        }

        public void OnMessageHasCome(string message)
        {
            _onMessageHasCome?.CallPersistentWithDynamicParameter(message);
        }
    }
}
