﻿using System;
using System.Threading.Tasks;

namespace TMGameTest
{
    public interface IChatServer
    {
        Task<ServerResult> SendMessage(string message);
        event Action<string> MessageRecieved;
    }
}