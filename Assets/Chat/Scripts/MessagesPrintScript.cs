using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace TMGameTest
{
    public class MessagesPrintScript : MonoBehaviour
    {
        [SerializeField]
        private Font _font;
        [SerializeField]
        private GameObject _messagePrefab;

        public void ClearAllContent()
        {
            for (int i = 0; i < transform.childCount; i++)
                if (transform.GetChild(i).gameObject.activeSelf)
                    Destroy(transform.GetChild(i).gameObject);
        }

        public void Print(string message)
        {
            var newGo = Instantiate(_messagePrefab);
            newGo.SetActive(true);
            newGo.transform.SetParent(transform);
            var rectTransform = newGo.GetComponent<RectTransform>();
            rectTransform.localScale = Vector3.one;

            var text = newGo.GetComponent<Text>();
            text.text = message;
            text.font = _font;

            GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 0f;
            StartCoroutine(DirtyHack());
        }

        private IEnumerator DirtyHack()
        {
            yield return null;
            var layoutGroup = GetComponent<VerticalLayoutGroup>();
            if (layoutGroup != null)
                layoutGroup.childForceExpandHeight = !layoutGroup.childForceExpandHeight;
            yield return null;
            if (layoutGroup != null)
                layoutGroup.childForceExpandHeight = !layoutGroup.childForceExpandHeight;
            var scrollRect = GetComponentInParent<ScrollRect>();
            if (scrollRect != null)
                scrollRect.verticalNormalizedPosition = 0f;
        }
    }
}
