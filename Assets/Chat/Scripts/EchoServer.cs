using System;
using System.Threading.Tasks;
using UnityEngine;

namespace TMGameTest
{
    [CreateAssetMenu(fileName = nameof(EchoServer), menuName = "CustomAssets/" + nameof(EchoServer), order = 0)]
    public class EchoServer : ScriptableObject, IChatServer
    {
        public event Action<string> MessageRecieved;

        Task<ServerResult> IChatServer.SendMessage(string message)
        {
            MessageRecieved?.Invoke(message);
            return Task.Run(() => ServerResult.OK);
        }
    }
}
