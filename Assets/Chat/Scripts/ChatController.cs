using UnityEngine;

namespace TMGameTest
{
    public class ChatController : MonoBehaviour
    {
        [SerializeField]
        private ScriptableObject _chatServer;
        [SerializeField]
        private UnityStringEvent _newMessageHasCome;
        [SerializeField]
        private UnityServerResultEvent _messageSendingResultHasCome;

        private void Start()
        {
            ((IChatServer)_chatServer).MessageRecieved += onMessageRecieved;
        }

        private void OnDestroy()
        {
            ((IChatServer)_chatServer).MessageRecieved -= onMessageRecieved;
        }

        private void onMessageRecieved(string message)
        {
            _newMessageHasCome?.Invoke(message);
        }

        public void Send(string message)
        {
            send(message);
        }

        private async void send(string message)
        {
            var result = await ((IChatServer)_chatServer).SendMessage(message);
            _messageSendingResultHasCome?.Invoke(result);
        }
    }
}
