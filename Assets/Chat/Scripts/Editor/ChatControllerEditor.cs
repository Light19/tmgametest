using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TMGameTest
{
    [CustomEditor(typeof(ChatController))]
    public class ChatControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_newMessageHasCome"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_messageSendingResultHasCome"));

            var obj = new MySerializedObject(target);

            var chatServer = obj.GetFieldValue<IChatServer>("_chatServer");
            var newServer = EditorGUILayout.ObjectField("Chat Server", (ScriptableObject)chatServer, typeof(IChatServer), false);
            if (newServer != chatServer)
            {
                obj.SetFieldValue("_chatServer", newServer);
                Undo.RecordObject(target, "Server set");
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
