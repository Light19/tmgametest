using System;
using System.Reflection;
using UnityEngine;

namespace TMGameTest
{
    public class MySerializedObject
    {
        private object _obj;
        private Type _type;

        private const BindingFlags _privateInstanceFlags = BindingFlags.Instance | BindingFlags.NonPublic;

        public MySerializedObject(object obj)
        {
            _obj = obj;
            _type = obj.GetType();
        }

        public T GetFieldValue<T>(string fieldName)
        {
            return (T)_type.GetField(fieldName, _privateInstanceFlags).GetValue(_obj);
        }

        public void SetFieldValue(string fieldName, object value)
        {
            _type.GetField(fieldName, _privateInstanceFlags).SetValue(_obj, value);
        }
    }
}
