﻿using System;
using UnityEditor;
using UnityEngine;

public class ScriptsFormatter : UnityEditor.AssetModificationProcessor
{
    public static void OnWillCreateAsset(string path)
    {
        path = path.Replace(".meta", "");
        var index = path.LastIndexOf(".");
        if (index < 0)
            return;
        var file = path.Substring(index);
        if (file != ".cs" && file != ".js" && file != ".boo") return;
        index = Application.dataPath.LastIndexOf("Assets");
        path = Application.dataPath.Substring(0, index) + path;
        file = System.IO.File.ReadAllText(path);

        var separator = "public class ";
        if (!file.Contains(separator))
            return;
        var arr = file.Split(new string[] { separator }, StringSplitOptions.None);
        arr[1] = "\r\n    " + separator + arr[1].Replace("\r\n{", "\r\n    {");
        file = string.Join("namespace " + PlayerSettings.productName + "\r\n{", arr) + "}\r\n";

        arr = file.Split(new string[] { "\r\n    // Start is" }, StringSplitOptions.None);
        file = arr[0] + "\r\n    }\r\n}\r\n";

        file = file.Replace("using System.Collections;\r\nusing System.Collections.Generic;\r\n", string.Empty);

        System.IO.File.WriteAllText(path, file);
        AssetDatabase.Refresh();
    }
}
