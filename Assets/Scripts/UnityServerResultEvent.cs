using System;
using UnityEngine.Events;

namespace TMGameTest
{
    [Serializable]
    public class UnityServerResultEvent : UnityEvent<ServerResult>
    {
    }
}
