using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

namespace TMGameTest
{
    public static class UnityBugsExtensions
    {
        private const BindingFlags EverythingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;

        public static void CallPersistentWithDynamicParameter(this UnityEventBase e, params object[] p)
        {
            var methodTargets = new List<Object>();
            var methodNames = new List<string>();
            for (int i = 0; i < e.GetPersistentEventCount(); i++)
            {
                methodTargets.Add(e.GetPersistentTarget(i));
                methodNames.Add(e.GetPersistentMethodName(i));
            }
            for (int i = 0; i < methodNames.Count; i++)
            {
                var target = methodTargets[i];
                var methodName = methodNames[i];
                var method = target.GetType().GetMethod(methodName, EverythingFlags);
                method.Invoke(target, p);
            }
        }
    }
}
