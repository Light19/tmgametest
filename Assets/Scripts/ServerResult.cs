﻿namespace TMGameTest
{
    public enum ServerResult
    {
        OK = 200,
        NotFound = 404
    }
}