using System.Xml;
using UnityEngine;

namespace TMGameTest
{
    public class MedalsTabs : MonoBehaviour
    {
        [SerializeField]
        private TextAsset _db;

        private void Start()
        {
            var doc = new XmlDocument();
            doc.LoadXml(_db.text);
            var root = doc.SelectSingleNode("root");
            var tabs = root.SelectSingleNode("tabs");

            var itabs = GetComponentInChildren<MedalTabsController>().CreateTabs(tabs, root);
            GetComponentInChildren<TabsView>().Initialize(itabs);
        }
    }
}
