using System;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

namespace TMGameTest
{
    public class Tab : MonoBehaviour, ITab
    {
        [SerializeField]
        private GameObject _medalPrefab;

        private string _name;
        private int _id;
        private Action _activate;
        private Action _deactivate;

        public void Initialize(string name, int id, Action activateAction, Action deactivateAction, XmlNode root)
        {
            _name = name;
            _id = id;
            _activate = activateAction;
            _deactivate = deactivateAction;

            foreach (XmlNode item in root)
            {
                if (item.Name != "item")
                    continue;
                var tabId = item.Attributes.GetNamedItem("tab_id").Value;
                if (Convert.ToInt32(tabId) != _id)
                    continue;

                var newMedal = Instantiate(_medalPrefab, _medalPrefab.transform.parent);
                var medalScript = newMedal.GetComponent<Medal>();
                medalScript.Initialize(item);
                newMedal.SetActive(true);
            }
        }

        string ITab.Name => _name;

        int ITab.Id => _id;

        void ITab.Activate()
        {
            _activate?.Invoke();
        }

        void ITab.Deactivate()
        {
            _deactivate?.Invoke();
        }
    }
}
