﻿namespace TMGameTest
{
    public interface ITab
    {
        string Name { get; }
        int Id { get; }

        void Deactivate();
        void Activate();
    }
}