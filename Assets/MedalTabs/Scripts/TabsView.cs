using UnityEngine;
using UnityEngine.UI;

namespace TMGameTest
{
    public class TabsView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _buttonPrefab;

        private int _currentTab = -1;
        private ITab[] _tabs;

        public void Initialize(ITab[] tabs)
        {
            _currentTab = 0;
            _tabs = tabs;

            createButtons(tabs);
        }

        private void createButtons(ITab[] tabs)
        {
            for (int i = 0; i < tabs.Length; i++)
            {
                var newButton = Instantiate(_buttonPrefab, _buttonPrefab.transform.parent);
                newButton.SetActive(true);
                var button = newButton.GetComponent<Button>();
                var cacheI = i;
                button.onClick.AddListener(() => gotoTab(cacheI));
                var text = newButton.GetComponentInChildren<Text>();
                text.text = tabs[i].Name;
            }
        }

        private void gotoTab(int index)
        {
            if (_currentTab >= 0)
                _tabs[_currentTab].Deactivate();
            _currentTab = index;
            _tabs[_currentTab].Activate();
        }
    }
}
