using System;
using System.Collections;
using System.Xml;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TMGameTest
{
    public class Medal : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private Text _nameText;
        [SerializeField]
        private UniGifImage _icon;
        [SerializeField]
        private Hint _hint;

        private XmlNode _medalNode;
        public void Initialize(XmlNode medalNode)
        {
            _medalNode = medalNode;

            _nameText.text = medalNode.SelectSingleNode("title").InnerText;
        }

        private void OnEnable()
        {
            var medalId = Convert.ToInt32(_medalNode.Attributes.GetNamedItem("id").Value);

            StartCoroutine(ViewGifCoroutine($"https://w0.tmgame.ru/files/awrd/{medalId}.gif"));
        }

        private IEnumerator ViewGifCoroutine(string url)
        {
            yield return StartCoroutine(_icon.SetGifFromUrlCoroutine(url));
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            _hint.Close(this);
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            _hint.Show(this, _medalNode.SelectSingleNode("stats"));
        }
    }
}
