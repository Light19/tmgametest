using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

namespace TMGameTest
{
    public class Hint : MonoBehaviour
    {
        [SerializeField]
        private Text _text;
        private Medal _currentMedal;

        internal void Show(Medal medal, XmlNode statsNode)
        {
            if (_currentMedal == null)
                gameObject.SetActive(true);

            if (medal != _currentMedal)
            {
                var sb = new StringBuilder();
                foreach (XmlNode stat in statsNode)
                {
                    sb.Append("  ");
                    sb.AppendLine(stat.InnerText);
                }
                sb.Remove(sb.Length - 2, 2);
                _text.text = sb.ToString();
                forceUnityToWorkProperly();
            }

            _currentMedal = medal;
        }

        private void forceUnityToWorkProperly()
        {
            Canvas.ForceUpdateCanvases();
            GetComponent<ContentSizeFitter>().enabled = false;
            GetComponent<ContentSizeFitter>().enabled = true;
        }

        internal void Close(Medal medal)
        {
            gameObject.SetActive(false);
            _currentMedal = null;
        }

        private void Update()
        {
            Vector2 pos;
            var canvas = GetComponentInParent<Canvas>();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out pos);
            transform.position = canvas.transform.TransformPoint(pos);
        }
    }
}
