using System;
using System.Xml;
using UnityEngine;

namespace TMGameTest
{
    public class MedalTabsController : MonoBehaviour
    {
        [SerializeField]
        private GameObject _tabPrefab;

        public ITab[] CreateTabs(XmlNode tabs, XmlNode root)
        {
            var tabsNodes = tabs.ChildNodes;
            var result = new ITab[tabsNodes.Count];
            var i = 0;
            foreach (XmlNode tab in tabs)
            {
                var newTab = Instantiate(_tabPrefab, _tabPrefab.transform.parent);
                var attributes = tab.Attributes;
                var id = Convert.ToInt32(attributes.GetNamedItem("id").Value);
                var name = attributes.GetNamedItem("title").Value;
                var tabMB = newTab.GetComponent<Tab>();
                tabMB.Initialize(name, id, () => newTab.SetActive(true), () => newTab.SetActive(false), root);
                result[i++] = tabMB;
            }
            return result;
        }
    }
}
